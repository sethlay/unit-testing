// IMPORT DEPENDENCIES
const chai = require("chai"); //assertion library
const {expect} = chai; //chai for style
const chaiHttp = require("chai-http"); //a request handler
chai.use(chaiHttp);

// SERVER URL
const serverUrl = "http://localhost:3000"

// TEST SUITE for GET ALL RATES
describe("GET ALL RATES", ()=>{
    it("should accept http requests", () => {
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err, res) => expect(res).to.not.equal(undefined))
    })

    it("should have a status of 200", (done) => {
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err,res) => {
            expect(res).to.have.status(200)
            done()
        })
    })

    it("should return an object w/ a property of rates", (done) => {
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err, res) => {
            expect(res.body).to.have.property('rates')
            done()
        })
    })

    it("should have an object w/ a property of rates w/ the length of 5", (done) => {
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err, res) => {
            expect(Object.keys(res.body.rates)).lengthOf(5)
            done()
        })
    })
})

// TEST SUITE for GET ALL CURRENCIES
describe("GET ALL CURRENCIES", () => {
    it("should accept http requests", (done) => {
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err, res) => expect(res).to.not.equal(undefined))
        done()
    })
    it("should have a status of 200", (done) => {
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err,res) => {
            expect(res).to.have.status(200)
            done()
        })
    })
    it("should have a property of currencies", (done) => {
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err, res) => {
            expect(res.body).to.have.property("currencies")
            done()
        })
    })
    it("should have a property of currencies with a length of 5", (done) => {
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err, res) => {
            expect(Object.keys(res.body.currencies)).lengthOf(5)
            done()
        })
    })
    it("should have a property of currencies that is an array", (done) => {
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err, res) => {
            expect(res.body.currencies).to.be.an("array")
            done()
        })
    })
})

describe('GET ONE CURRENCY', () => {
    describe("THAT IS USD", () => {
        it("should have status 200", () => {
            chai
            .request(serverUrl)
            .get("/currencies/usd")
            .end((err, res) => expect(res).to.have.status(200))
        })
        it("should have property of usd", (done) => {
            chai
            .request(serverUrl)
            .get("/currencies/usd")
            .end((err, res, req) => {
                expect(res.body).to.have.property("usd")
                done()
            })
        })
        it("should have a nested property called name", (done) => {
            chai
            .request(serverUrl)
            .get("/currencies/usd")
            .end((err, res) => {
                expect(res.body.usd).to.have.property("name")
                done()
            })

        })
    })
})