// IMPORT DEPENDENCY 
const { expect } = require("chai");

// IMPORT FUNCTIONS
const { exchange, exchangeRates } = require("../src/utils");

// // TEST
// it("test_exchange_usd_to_php", () => {
//     const phpValue = exchange("usd", "peso", 1000);
//     expect(phpValue).to.equal(50730)
// })

// // ACTIVITY
// it("test_exchange_usd_to_yen", () => {
//     const yenValue = exchange("usd", "yen", 1000);
//     expect(yenValue).to.equal(108630);
// })

// it("test_exchange_usd_to_yuan", () => {
//     const yuanValue = exchange("usd", "yuan", 1000);
//     expect(yuanValue).to.equal(7030);
// })

// it("test_exchange_usd_to_won", () => {
//     const wonValue = exchange("usd", "won", 1000);
//     expect(wonValue).to.equal(1187240)
// })

// // stretch goal
// it("test_exchange_yen_to_peso", () => {
//     const phpValue = exchange("yen", "peso", 1000);
//     expect(phpValue).to.equal(740)
// })

// it("test_exchange_yen_to_usd", () => {
//     const usdValue = exchange("yen", "usd", 1000);
//     expect(usdValue).to.equal(9.2)
// })

// it("test_exchange_yen_to_won", () => {
//     const wonValue = exchange("yen", "won", 1000);
//     expect(wonValue).to.equal(10930)
// })

// it("test_exchange_yen_to_yuan", () => {
//     const yuanValue = exchange("yen", "yuan", 1000);
//     expect(yuanValue).to.equal(65)
// })

// it("test_exchange_peso_to_usd", () => {
//     const usdValue = exchange("peso", "usd", 1000);
//     expect(usdValue).to.equal(20)
// })

// it("test_exchange_peso_to_won", () => {
//     const wonValue = exchange("peso", "won", 1000);
//     expect(wonValue).to.equal(23390)
// })

// it("test_exchange_peso_to_yen", () => {
//     const yenValue = exchange("peso", "yen", 1000);
//     expect(yenValue).to.equal(2140)
// })

// it("test_exchange_peso_to_yuan", () => {
//     const yuanValue = exchange("peso", "yuan", 1000);
//     expect(yuanValue).to.equal(140)
// })

// it("test_exchange_yuan_to_peso", () => {
//     const pesoValue = exchange("yuan", "peso", 1000);
//     expect(pesoValue).to.equal(7210)
// })

// it("test_exchange_yuan_to_usd", () => {
//     const usdValue = exchange("yuan", "usd", 1000);
//     expect(usdValue).to.equal(140)
// })

// it("test_exchange_yuan_to_won", () => {
//     const wonValue = exchange("yuan", "won", 1000);
//     expect(wonValue).to.equal(168850)
// })

// it("test_exchange_yuan_to_yen", () => {
//     const yenValue = exchange("yuan", "yen", 1000);
//     expect(yenValue).to.equal(15450)
// })

// it("test_exchange_won_to_peso", () => {
//     const pesoValue = exchange("won", "peso", 1000);
//     expect(pesoValue).to.equal(43)
// })

// it("test_exchange_won_to_usd", () => {
//     const usdValue = exchange("won", "usd", 1000);
//     expect(usdValue).to.equal(0.8400000000000001)
// })

// it("test_exchange_won_to_yen", () => {
//     const yenValue = exchange("won", "yen", 1000);
//     expect(yenValue).to.equal(92)
// })

// it("test_exchange_won_to_yuan", () => {
//     const yuanValue = exchange("won", "yuan", 1000);
//     expect(yuanValue).to.equal(5.8999999999999995)
// })


// TEST SUITE DEMO
// mocha -> unit testing framework
    // describe() -> is not part of javascript, but rather it is a function defined in the library that we used (mocha).
    // --> is a function in the Mocha testing framework. It simply describes the suite of test cases enumerated by the "it" function. --> Also used in the Jasmine Framework.
    // -->describing only the functions in one group or one test suite family

// EASY FIXES FOR UNIT TESTING(TECH EXAMS)
//  -> Make sure that the script is correct (routes, module exports, typos, incorrect syntaxes)
//  -> Use the proper syntaxes in the Gitbash or Terminal
//  -> If you are not using Nodemon - restart server
    // -> close the terminal and open it again
    // GO BACK TO THE FIRST POINTER

describe('test_exchange_usd_to_multiple_currencies', () => {
    // convert usd to php
    it('test_exchange_usd_to_php', () => {
        const phpValue = exchange('usd', 'peso', 1000)
        expect(phpValue).to.equal(50730)
    })

    // usd to yen
    it('test_exchange_usd_to_yen', () => {
        const yenValue = exchange('usd', 'yen', 1000)
        expect(yenValue).to.equal(108630)
    })

    // usd to yuan
    it("test_exchange_usd_to_yuan", () => {
    const yuanValue = exchange("usd", "yuan", 1000);
    expect(yuanValue).to.equal(7030);
    })

    // usd to won
    it("test_exchange_usd_to_won", () => {
        const wonValue = exchange("usd", "won", 1000);
        expect(wonValue).to.equal(1187240)
    })
})

// ACTIVITY for SUITE DEMO
describe('test_exchange_peso_to_multiple_currencies', () => {
    it("test_exchange_peso_to_usd", () => {
    const usdValue = exchange("peso", "usd", 1000);
    expect(usdValue).to.equal(20)
    })

    it("test_exchange_peso_to_won", () => {
        const wonValue = exchange("peso", "won", 1000);
        expect(wonValue).to.equal(23390)
    })

    it("test_exchange_peso_to_yen", () => {
        const yenValue = exchange("peso", "yen", 1000);
        expect(yenValue).to.equal(2140)
    })

    it("test_exchange_peso_to_yuan", () => {
        const yuanValue = exchange("peso", "yuan", 1000);
        expect(yuanValue).to.equal(140)
    })
})
