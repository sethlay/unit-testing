const express = require('express');
const router = express.Router();
const {exchangeRates} = require("../src/utils.js");

// GET ALL RATES
router.get('/', (req, res)=>{
    return res.send({"rates": Object.keys(exchangeRates)})
})

module.exports = router;