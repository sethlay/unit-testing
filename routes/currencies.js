const express = require('express');
const router = express.Router();
const {exchangeRates} = require("../src/utils");

// GET ALL CURRENCIES
router.get("/", (req, res)=>{
    return res.send({"currencies": Object.keys(exchangeRates)})
})

// GET ONE
router.get("/:currency", (req, res) => {
    return res.send({ [req.params.currency] : exchangeRates[req.params.currency] })
})

module.exports = router;